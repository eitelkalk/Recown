package de.eitelkalk.pewn;

import de.eitelkalk.recown.Item;

public class PewnItem extends Item {

	private final String name;
	private final long id;

	public PewnItem(String name, long id) {
		this.name = name;
		this.id = id;
	}

	public long getID() {
		return id;
	}

	public String getName() {
		return name;
	}
}