package de.eitelkalk.recown.tools;

/**
 * This class provides frequently called methods for general purposes.
 * 
 * @author eitelkalk
 *
 */
public class Tools {

	/**
	 * Linearly maps the value {@code x} from the interval
	 * {@code [minStart, maxStart]} to the corresponding value in the interval
	 * {@code [minTarget, maxTarget]}. Assumes that
	 * {@code minStart <= x <= maxStart} and {@code minTarget <= maxTarget}.
	 * 
	 * <p>
	 * If {@code minStart == maxStart}, then {@code minTarget} is returned.
	 * </p>
	 * 
	 * @param x
	 *            the value that should be mapped
	 * @param minStart
	 *            lower bound of the start interval
	 * @param maxStart
	 *            upper bound of the start interval
	 * @param minTarget
	 *            lower bound of the target interval
	 * @param maxTarget
	 *            upper bound of the target interval
	 * @return {@code y} in the interval {@code [minTarget, maxTarget]}
	 *         corresponding to the value {@code x} in the interval
	 *         {@code [minStart, maxStart]}.
	 */
	public static double mapToInterval(double x, double minStart, double maxStart, double minTarget,
			double maxTarget) {
		if (x < minStart || x > maxStart || minTarget > maxTarget || minStart > maxStart) {
			throw new IllegalArgumentException(
					"Passed values must satify minStart <= x <= maxStart and minTarget <= maxTarget.");
		}
		if (maxStart - minStart == 0) {
			return minTarget;
		}
		return (x - minStart) / (maxStart - minStart) * (maxTarget - minTarget) + minTarget;
	}
	
	public static double keepBetween(double value, double min, double max) {
		return Math.min(max, Math.max(value, min));
	}
}
