package de.eitelkalk.recown.tools;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import de.eitelkalk.recown.Similar;

/**
 * Represents a sorted list sorting the elements by their
 * {@link Similar#similarityTo(Object)} some given root element. The maximum
 * number of elements can be fixed, i.&nbsp;e. the maximum number of elements
 * stored in that list.
 */
public class SortedList<T extends Similar<T>> implements Iterable<T> {

	private final List<T> elements = new ArrayList<T>();
	private final T root;
	private final int maxSize;

	/**
	 * Creates a list that sorts the subsequent elements by their
	 * {@link Similar#similarityTo(Object)} the {@code root} element from
	 * identical ({@code similarity == 1} to nothing in common
	 * ({@code similarity == 0}).
	 * 
	 * <p>
	 * The {@code root} element is only used for the similarity calculation and
	 * not contained in the list itself. If it should be stored in the list, you
	 * would have to {@link #add(Similar)} it explicitly.
	 * </p>
	 * 
	 * @param root
	 *            the element with which all elements added to this list will be
	 *            compared with.
	 */
	public SortedList(T root) {
		this(root, Integer.MAX_VALUE);
	}

	/**
	 * Creates a list that sorts the subsequent elements by their
	 * {@link Similar#similarityTo(Object)} the {@code root} element from
	 * identical ({@code similarity == 1} to nothing in common
	 * ({@code similarity == 0}). It can store at most {@code maxNumberOfElements}
	 * elements.
	 * 
	 * <p>
	 * The {@code root} element is only used for the similarity calculation and
	 * not contained in the list itself. If it should be stored in the list, you
	 * would have to {@link #add(Similar)} it explicitly.
	 * </p>
	 * 
	 * @param root
	 *            the element with which all elements added to this list will be
	 *            compared with.
	 * @param maxNumberOfElements
	 *            the maximum number of elements that will be stored in that
	 *            list which will never be exceeded.
	 */
	public SortedList(T root, int maxNumberOfElements) {
		this.root = root;
		this.maxSize = maxNumberOfElements < 0 ? 0 : maxNumberOfElements;
	}

	/**
	 * Adds an {@code element} to the list at an index, such that the sorted
	 * structure is maintained. This includes finding an appropriate index where
	 * the {@code element} should be inserted.
	 * 
	 * <p>
	 * If the similarity is smaller than the similarity of the last element in the
	 * list and the maximum number of elements is already reached, then this
	 * element isn't added at all.
	 * </p>
	 * 
	 * <p>
	 * If this element is inserted at some index and the maximum number of
	 * elements is already reached, then the last element will be removed to not
	 * exceed the maximum number of elements.
	 * </p>
	 * 
	 * @param element
	 *            the element to add to the list.
	 */
	public void add(T element) {
		if (element == null)
			return;

		int index = getSuitableIndex(element);
		if (index >= 0) {
			elements.add(index, element);
		}
		while (elements.size() > maxSize) {
			elements.remove(elements.size() - 1);
		}
	}

	private int getSuitableIndex(T element) {
		T tmp;
		double similarity = element.similarityTo(root);
		double difference;
		double rand = 0.5;
		for (int i = 0; i < maxSize; i++) {
			if (i == elements.size()) {
				return elements.size();
			}

			tmp = elements.get(i);
			difference = similarity - tmp.similarityTo(root);
			if (difference > 0) {
				return i;
			} else if (difference == 0) {
				if (Math.random() > rand) {
					return i;
				} else {
					rand /= 2;
				}
			} else {
				rand = 0.5;
			}
		}
		return -1;
	}

	public List<T> toList() {
		return elements;
	}

	@Override
	public Iterator<T> iterator() {
		return elements.iterator();
	}
}